const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

app.use(express.static(`views/Pizza_365`)); // Use this for show image

app.get("/", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/Pizza_365/43.80.html`));
})

 
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
